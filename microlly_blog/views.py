from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from microlly_blog import forms
from microlly_blog.models import Post

# Create your views here.


def index(request):
    posts_list = Post.objects.all().order_by('-release_date')
    paginator = Paginator(posts_list, 9)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request, 'microlly_blog/index.html', {'latest_posts': posts})

@login_required(login_url="/accounts/login/")
def post_create(request):
    if request.method == 'POST':
        form = forms.PostCreate(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('microlly_blog:index')
    else:
        form = forms.PostCreate()
    return render(request, 'microlly_blog/post_create.html', {'form': form})

def post_details(request, slug, pk):
    post = get_object_or_404(Post, slug=slug, pk=pk)
    return render(request, 'microlly_blog/details.html', {'post': post})


@login_required(login_url="/accounts/login/")
def post_edit(request, slug, pk):
    post = get_object_or_404(Post, slug=slug, pk=pk)
    form = forms.PostEdit(request.POST or None, instance=post)
    if request.user != post.author:
        return redirect('microlly_blog:index')
    if request.method == 'POST':
        if form.is_valid():
            post.save()
            return redirect('microlly_blog:index')
    context = {
        "form": form,
        "post": post,
    }
    return render(request, 'microlly_blog/post_edit.html', context)

def list_post_user(request, author):                    # Affichage des posts
    author = get_object_or_404(User, username=author)   # Récupère l'auteur
    posts_list = Post.objects.filter(author=author)     # Récupère les posts
    paginator = Paginator(posts_list, 9)                # Mise en page
    page = request.GET.get('page')
    posts = paginator.get_page(page)

    context = {
        "author": author,
        "list_posts_user": posts,
    }
    return render(request, 'microlly_blog/list_posts_user.html', context)

def post_delete(request, slug, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.user != post.author:                 # Vérifie que auteur=utilisateur connecter sinon il peut pas le supprimer
        return redirect('microlly_blog:index')
    if request.method == 'POST':
        post.delete()
        return redirect('microlly_blog:index')
