from django.contrib import admin
from microlly_blog.models import *

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    list_view = ('title', 'release_date', 'edit_date',)
    list_filter = ('release_date', 'edit_date', 'author')
    research_fields = ('title',)


admin.site.register(Post, PostAdmin)
