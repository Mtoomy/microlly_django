from django.urls import path

from microlly_blog import views

app_name = 'microlly_blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.post_create, name='create'),
    path('post_by/<str:author>', views.list_post_user, name='list_post_user'),
    path('delete/<slug:slug>/<int:pk>', views.post_delete, name='delete'),
    path('post/<slug:slug>/<int:pk>', views.post_details, name='details'),
    path('edit/<slug:slug>/<int:pk>', views.post_edit, name='edit'),
]
