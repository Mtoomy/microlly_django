from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from accounts import forms


# Create your views here.


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST: 
                return redirect(request.POST.get('next'))
            else: #Renvoie sur la page d'acceuil
                return redirect('microlly_blog:index')
    else:  #Renvoie sur la page de connexion
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form': form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('microlly_blog:index')
    else:
        return redirect('microlly_blog:index')

def signup_view(request):
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('microlly_blog:index')
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form': form})
