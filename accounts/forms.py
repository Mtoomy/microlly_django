from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    forename = forms.CharField(max_length=30, required=False, help_text='Optional./Optionnel.')
    surname = forms.CharField(max_length=30, required=False, help_text='Optional./Optionnel.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address./Requis. Veuillez saisir une addresse e-mail valide.')

    class Meta:
        model = User
        fields = ('username', 'forename', 'surname',
                  'email', 'password1', 'password2',)
